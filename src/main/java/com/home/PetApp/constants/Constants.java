package com.home.PetApp.constants;

public class Constants {
    public static final String GOLDFISH = "GOLD_FISH";
    public static final String HAMSTER = "HAMSTER";
    public static final String CAT = "CAT";
    public static final String DOG = "DOG";
}
