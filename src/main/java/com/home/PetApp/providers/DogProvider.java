package com.home.PetApp.providers;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.home.PetApp.annotations.*;
import com.home.PetApp.services.Dog;
import com.home.PetApp.services.HomePet;

public class DogProvider implements Provider<HomePet> {
    private String color;
    private Integer repeatTime;

    @Inject
    public DogProvider(@DogColorValue String color, @DogRepeatValue Integer repeatTime) {
        this.color = color;
        this.repeatTime = repeatTime;
    }

    @Override
    public HomePet get() {
        Dog d = new Dog(repeatTime, color);
        return d;
    }

}
