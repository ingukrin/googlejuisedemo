package com.home.PetApp.providers;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.home.PetApp.annotations.CatColorValue;
import com.home.PetApp.annotations.CatRepeatValue;
import com.home.PetApp.services.Cat;
import com.home.PetApp.services.HomePet;

public class CatProvider implements Provider<HomePet> {
    private String color;
    private Integer repeatTime;

    @Inject
    public CatProvider(@CatColorValue String color, @CatRepeatValue Integer repeatTime) {
        this.color = color;
        this.repeatTime = repeatTime;
    }

    @Override
    public HomePet get() {
        Cat d = new Cat(repeatTime, color);
        return d;
    }

}
