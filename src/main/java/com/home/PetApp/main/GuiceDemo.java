package com.home.PetApp.main;

import com.google.inject.AbstractModule;
import com.home.PetApp.module.CatModule;
import com.home.PetApp.module.DogModule;
import com.home.PetApp.module.GoldFishModule;
import com.home.PetApp.module.PetModule;
import com.home.PetApp.request.*;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.home.PetApp.services.Hamster;
//import com.home.PetApp.request.HamsterRequest;
//import com.home.PetApp.services.Hamster;
import java.util.ArrayList;
import java.util.List;

import static com.home.PetApp.constants.Constants.GOLDFISH;
import static com.home.PetApp.constants.Constants.DOG;
import static com.home.PetApp.constants.Constants.CAT;
import static com.home.PetApp.constants.Constants.HAMSTER;

public class GuiceDemo {
     //private static  final String ANIMAL_TYPE=CAT;
    //private static  final String ANIMAL_TYPE="BAD_CAT";
     private static  final String ANIMAL_TYPE=GOLDFISH;
    //private static  final String ANIMAL_TYPE=HAMSTER;
//    private static  final String ANIMAL_TYPE=DOG;
     //private static  final String ANIMAL_TYPE="ALL";

    private static void sendRequest(String animalType) {


        if (animalType.equals(CAT)) {
            Injector injector = Guice.createInjector(new CatModule(), new DogModule(), new GoldFishModule());
            CatRequest catRequest = injector.getInstance(CatRequest.class);
            catRequest.requestSound();
        }

//        if (animalType.equals("BAD_CAT")) {
//            Injector injector = Guice.createInjector((new PetModule()));
//            CatRequestSubclassDadCat catRequest = injector.getInstance(CatRequestSubclassDadCat.class);
//            catRequest.requestSound();
//        }

        List<AbstractModule> modules = new ArrayList<>();
        modules.add(new DogModule());
        modules.add(new CatModule());
        modules.add(new GoldFishModule());

        if (animalType.equals(DOG)) {
            Injector injector = Guice.createInjector(modules);
            DogRequest dogRequest = injector.getInstance(DogRequest.class);
            dogRequest.requestSound();
        }

        if (animalType.equals(HAMSTER)) {
            Injector injector = Guice.createInjector((new PetModule()));
            HamsterRequest hamsterRequest = injector.getInstance(HamsterRequest.class);
            hamsterRequest.requestSound();
        }

        if (animalType.equals(GOLDFISH)) {
            Injector injector = Guice.createInjector((new GoldFishModule()));
            HomePetRequest homePetRequest = injector.getInstance(HomePetRequest.class);
            homePetRequest.makeRequests(GOLDFISH);
        }

    }
    public static void main(String[] args) {
        sendRequest(ANIMAL_TYPE);
    }

}
