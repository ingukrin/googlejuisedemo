package com.home.PetApp.module;


import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.multibindings.Multibinder;
import com.home.PetApp.annotations.*;
import com.home.PetApp.providers.CatProvider;
import com.home.PetApp.providers.DogProvider;
import com.home.PetApp.services.GoldFish;
import com.home.PetApp.services.HomePet;

public class GoldFishModule extends AbstractModule {


    @Override
    protected void configure(){
        bind(HomePet.class).annotatedWith(GoldFishClass.class).to(GoldFish.class).in(Scopes.SINGLETON);
        bind(String.class).annotatedWith(GoldFishColorValue.class).toInstance("Gold");
        bind(Integer.class).annotatedWith(GoldFishRepeatValue.class).toInstance(1);

        Multibinder<HomePet> homePetBinder = Multibinder.newSetBinder(binder(),HomePet.class);
        homePetBinder.addBinding().to(GoldFish.class) ;
//        homePetBinder.addBinding().to(Cat.class) ;
//        homePetBinder.addBinding().to(Dog.class) ;
    }
}
