package com.home.PetApp.module;


import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.multibindings.Multibinder;
import com.home.PetApp.annotations.*;
import com.home.PetApp.providers.CatProvider;
import com.home.PetApp.providers.DogProvider;
import com.home.PetApp.services.GoldFish;
import com.home.PetApp.services.HomePet;

public class HamsterModule extends AbstractModule {


    @Override
    protected void configure(){
        bind(String.class).annotatedWith(HamsterColorValue.class).toInstance("Yellow");
        bind(Integer.class).annotatedWith(HamsterRepeatValue.class).toInstance(3);


    }
}
