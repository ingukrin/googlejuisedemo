package com.home.PetApp.module;


import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.multibindings.Multibinder;
import com.home.PetApp.annotations.*;
import com.home.PetApp.providers.CatProvider;
import com.home.PetApp.providers.DogProvider;
import com.home.PetApp.services.GoldFish;
import com.home.PetApp.services.HomePet;

public class CatModule extends AbstractModule {


    @Override
    protected void configure(){
//        bind(HomePet.class).annotatedWith(CatClass.class).to(Cat.class).in(Scopes.SINGLETON);
        bind(HomePet.class).annotatedWith(CatClass.class).toProvider(CatProvider.class).in(Scopes.SINGLETON);
        bind(String.class).annotatedWith(CatColorValue.class).toInstance("Grey");
        bind(Integer.class).annotatedWith(CatRepeatValue.class).toInstance(10);


    }
}
