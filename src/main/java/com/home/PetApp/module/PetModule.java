package com.home.PetApp.module;


import com.google.inject.Scopes;
import com.google.inject.multibindings.Multibinder;
import com.home.PetApp.annotations.*;
import com.home.PetApp.providers.CatProvider;
import com.home.PetApp.providers.DogProvider;
import com.home.PetApp.services.Cat;
import com.home.PetApp.services.Dog;
import com.home.PetApp.services.GoldFish;
import com.home.PetApp.services.HomePet;
import com.google.inject.AbstractModule;

public class PetModule extends AbstractModule {


    @Override
    protected void configure(){
        install(new CatModule());
        install(new DogModule());
        install(new HamsterModule());
        install(new GoldFishModule());
//        bind(HomePet.class).annotatedWith(CatClass.class).to(Cat.class).in(Scopes.SINGLETON);
//        bind(HomePet.class).annotatedWith(DogClass.class).to(Dog.class).in(Scopes.SINGLETON);
//        bind(HomePet.class).annotatedWith(CatClass.class).toProvider(CatProvider.class).in(Scopes.SINGLETON);
//        bind(String.class).annotatedWith(CatColorValue.class).toInstance("Grey");
//        bind(Integer.class).annotatedWith(CatRepeatValue.class).toInstance(10);
//
//        bind(HomePet.class).annotatedWith(DogClass.class).toProvider(DogProvider.class).in(Scopes.SINGLETON);
//        bind(String.class).annotatedWith(DogColorValue.class).toInstance("Black");
//        bind(Integer.class).annotatedWith(DogRepeatValue.class).toInstance(5);
//
//        bind(String.class).annotatedWith(HamsterColorValue.class).toInstance("Yellow");
//        bind(Integer.class).annotatedWith(HamsterRepeatValue.class).toInstance(3);
//
//
//        bind(HomePet.class).annotatedWith(GoldFishClass.class).to(GoldFish.class).in(Scopes.SINGLETON);
//        bind(String.class).annotatedWith(GoldFishColorValue.class).toInstance("Gold");
//        bind(Integer.class).annotatedWith(GoldFishRepeatValue.class).toInstance(1);
//
//        Multibinder<HomePet> homePetBinder = Multibinder.newSetBinder(binder(),HomePet.class);
//        homePetBinder.addBinding().to(GoldFish.class) ;
//        homePetBinder.addBinding().to(Cat.class) ;
//        homePetBinder.addBinding().to(Dog.class) ;

    }

//    @Provides
//    @Singleton
//    @DogClass
//    HomePet providesDog(@DogRepeatTime Integer dogRepeatTime, @DogColor String dogColor){
//        HomePet homePet = new Dog(dogRepeatTime, dogColor);
//        return homePet;
//    }
//
//    @Provides
//    @Singleton
//    @CatClass
//    HomePet providesCat(@EdgeValue Integer repeatTime, @ColorValue String color){
//        HomePet homePet = new Cat(repeatTime, color);
//        return homePet;
//    }


}
