package com.home.PetApp.module;


import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.multibindings.Multibinder;
import com.home.PetApp.annotations.*;
import com.home.PetApp.providers.CatProvider;
import com.home.PetApp.providers.DogProvider;
import com.home.PetApp.services.GoldFish;
import com.home.PetApp.services.HomePet;

public class DogModule extends AbstractModule {


    @Override
    protected void configure(){

//        bind(HomePet.class).annotatedWith(DogClass.class).to(Dog.class).in(Scopes.SINGLETON);
        bind(HomePet.class).annotatedWith(DogClass.class).toProvider(DogProvider.class).in(Scopes.SINGLETON);
        bind(String.class).annotatedWith(DogColorValue.class).toInstance("Black");
        bind(Integer.class).annotatedWith(DogRepeatValue.class).toInstance(5);
    }
}
