package com.home.PetApp.request;

import com.google.inject.Provider;
import com.home.PetApp.services.Hamster;
import javax.inject.Inject;

public class HamsterRequest {
    Provider<Hamster> hamsterProvider;

    @Inject
    public HamsterRequest(Provider<Hamster> hamsterProvider)
    {
        this.hamsterProvider = hamsterProvider;
    }

    public void requestSound()
    {
        Hamster hamster1 = hamsterProvider.get();
        hamster1.makeSound();
        Hamster hamster2 = hamsterProvider.get();
        hamster2.makeSound();
    }
}
