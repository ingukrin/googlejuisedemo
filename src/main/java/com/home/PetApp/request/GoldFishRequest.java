package com.home.PetApp.request;

import com.home.PetApp.annotations.CatClass;
import com.home.PetApp.annotations.GoldFishClass;
import com.home.PetApp.services.HomePet;

import javax.inject.Inject;


public class GoldFishRequest {
    HomePet pet;


    @Inject
    //public GoldFishRequest(HomePet pet)
    public GoldFishRequest(@GoldFishClass HomePet pet)
    {
        this.pet = pet;
    }
    public void requestSound()
    {
        pet.makeSound();
    }


}
