package com.home.PetApp.request;

import com.home.PetApp.services.HomePet;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HomePetRequest {
    Map<String, HomePet> homePetNameVSHPMEPet;

    @Inject
    public HomePetRequest(Set<HomePet> set) {
        homePetNameVSHPMEPet = new HashMap<String, HomePet>();
        for (HomePet d : set) {
            homePetNameVSHPMEPet.put(d.getHomePetName(), d);
        }
    }
    public void makeRequests(String homePetName){
        HomePet d =homePetNameVSHPMEPet.get(homePetName);
        d.makeSound();
    }
}
