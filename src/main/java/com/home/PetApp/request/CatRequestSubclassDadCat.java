package com.home.PetApp.request;

import com.home.PetApp.annotations.CatClass;
import com.home.PetApp.services.HomePet;

import javax.inject.Inject;

public class CatRequestSubclassDadCat extends CatRequest {

    @Inject
    public CatRequestSubclassDadCat(@CatClass HomePet pet) {
        super(pet);
    }



    @Override
    public void requestSound(){
        System.out.println("Hhhhhhh Hhhhhh Hhhhhh");
        pet.makeSound();
    }
}
