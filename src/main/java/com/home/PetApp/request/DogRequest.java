package com.home.PetApp.request;

import com.home.PetApp.annotations.DogClass;
import com.home.PetApp.services.HomePet;

import javax.inject.Inject;


public class DogRequest {
    //    @Inject
    HomePet pet;


    @Inject
    //public DogRequest(HomePet pet)
    public DogRequest(@DogClass HomePet pet)
    {
        this.pet = pet;
    }




////Method injection
//    @Inject
//    public void setPet(HomePet pet) {
//        this.pet = pet;
//    }

    public void requestSound(){
        pet.makeSound();
    }

    public HomePet getPet() {
        return pet;
    }
}
