package com.home.PetApp.request;

import com.home.PetApp.annotations.CatClass;
import com.home.PetApp.services.HomePet;

import javax.inject.Inject;


public class CatRequest {
    //    @Inject
    HomePet pet;


    @Inject
    //public CatRequest(HomePet pet)
    public CatRequest(@CatClass HomePet pet)
    {
        this.pet = pet;
    }


////Method injection
//    @Inject
//    public void setPet(HomePet pet) {
//        this.pet = pet;
//    }

    public void requestSound(){
        pet.makeSound();
    }

    public HomePet getPet() {
        return pet;
    }
}
