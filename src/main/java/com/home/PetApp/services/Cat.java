package com.home.PetApp.services;

import com.home.PetApp.annotations.CatColorValue;
import com.home.PetApp.annotations.CatRepeatValue;
import static com.home.PetApp.constants.Constants.CAT;
import javax.inject.Inject;

public class Cat implements HomePet{
    private Integer repeatTime;
    private String color;

    @Inject
    public Cat(Integer repeatTime, String color) {
//    public Cat(@CatRepeatValue Integer repeatTime, @CatColorValue String color) {
        this.repeatTime = repeatTime;
        this.color =color;
    }


    @Override
    public void makeSound() {
        for (int i=1;i <=repeatTime; i++){
            System.out.print("Meow is "+color+ " cat; ");
        }
        System.out.println();
    }

    @Override
    public String getHomePetName() {
        return CAT;
    }
}
