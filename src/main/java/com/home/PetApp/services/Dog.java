package com.home.PetApp.services;

import javax.inject.Inject;
import static com.home.PetApp.constants.Constants.DOG;

public class Dog implements HomePet{
    private Integer repeatTime;
    private String dogColor;

    @Inject
    public Dog(Integer repeatTime, String dogColor) {
    //public Dog(@DogRepeatTime Integer repeatTime, @DogColor String dogColor) {
        super();
        this.repeatTime = repeatTime;
        this.dogColor =dogColor;
    }

    @Override
    public void makeSound() {

        for (int i=1;i<=repeatTime;i++){
            System.out.println("Vau is: " + dogColor);
        }
        System.out.println();
    }

    @Override
    public String getHomePetName() {
        return DOG;
    }
}
