package com.home.PetApp.services;

public interface HomePet {
    void makeSound();
    String getHomePetName();
}
