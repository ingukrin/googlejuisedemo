package com.home.PetApp.services;

import com.home.PetApp.annotations.HamsterColorValue;
import com.home.PetApp.annotations.HamsterRepeatValue;
import static com.home.PetApp.constants.Constants.HAMSTER;
import javax.inject.Inject;

public class Hamster implements HomePet{
    private Integer hamsterRepeatTime;
    private String hamsterColor;
    Thread t = new Thread(()-> System.out.println("Uiiiiii "+ hamsterColor+"-->"+ hamsterRepeatTime));

    @Inject
    public Hamster(@HamsterRepeatValue Integer hamsterRepeatTime, @HamsterColorValue String hamsterColor) {
        this.hamsterRepeatTime = hamsterRepeatTime;
        this.hamsterColor =hamsterColor;
    }

    @Override
    public void makeSound() {
        t.start();
    }

    @Override
    public String getHomePetName() {
        return HAMSTER;
    }
}
