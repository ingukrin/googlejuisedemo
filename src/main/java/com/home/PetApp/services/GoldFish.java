package com.home.PetApp.services;

import com.home.PetApp.annotations.GoldFishColorValue;
import com.home.PetApp.annotations.GoldFishRepeatValue;
import com.home.PetApp.annotations.HamsterColorValue;
import com.home.PetApp.annotations.HamsterRepeatValue;
import static com.home.PetApp.constants.Constants.GOLDFISH;

import javax.inject.Inject;

public class GoldFish implements HomePet{
    private Integer goldFishRepeateTime;
    private String goldFishColor;

    Thread t = new Thread(()-> System.out.println("Buļ buļ bul "+ goldFishColor+"-->"+ goldFishRepeateTime));

    @Inject
    public GoldFish(@GoldFishRepeatValue Integer goldFishRepeateTime, @GoldFishColorValue String goldFishColor) {
        this.goldFishRepeateTime = goldFishRepeateTime;
        this.goldFishColor =goldFishColor;
    }

    @Override
    public void makeSound() {
        t.start();
    }


    @Override
    public String getHomePetName() {
        return GOLDFISH;
    }
}
